var Knight = /** @class */ (function () {
    function Knight(stage) {
        this.sprite = new PIXI.Sprite();
        this.knight = new PIXI.Sprite(resources["../../src/knight_run left_0.png"].texture);
        stage.addChild(this.knight);
        this.knight.x = 220;
        this.knight.y = 400;
    }
    return Knight;
}());
