class Knight {
    private sprite = new PIXI.Sprite();
    private knight = new PIXI.Sprite(resources["../../src/knight_run left_0.png"].texture);

    constructor(stage: PIXI.Container) {
        stage.addChild(this.knight);
        this.knight.x = 220;
        this.knight.y = 400;
    }
}