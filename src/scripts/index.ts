import {Base} from "base.js";
import {Knight} from "knight.js";

// CREATING A PIXI APP
const app = new PIXI.Application({
        width: 512,
        height: 512,
        antialias: true,
        transparent: false,
        resolution: 1
    }
);
document.body.appendChild(app.view);

const KNIGHT_FRAME_LIST_LEFT = [
    "images/knight_run left_0.png",
    "images/knight_run left_1.png",
    "images/knight_run left_2.png",
    "images/knight_run left_3.png",
    "images/knight_run left_4.png",
    "images/knight_run left_5.png",
];
const KNIGHT_FRAME_LIST_RIGHT = [
    "images/knight_run right_0.png",
    "images/knight_run right_1.png",
    "images/knight_run right_2.png",
    "images/knight_run right_3.png",
    "images/knight_run right_4.png",
    "images/knight_run right_5.png",
];
const FOOD = [
    "images/Food.png"
];
const BASE = [
    "images/layer-22.png"
];
const GAME_OVER = [
    "images/OVER.png"
];
// LOADER
loader
    .add([
        KNIGHT_FRAME_LIST_LEFT,
        KNIGHT_FRAME_LIST_RIGHT,
        FOOD,
        BASE,
        GAME_OVER,
    ])
    .load(setup);

function setup() {

}